## 1) Introduction
**Que veut faire Guillaume avec les fichiers musicaux de son amie ?**


|Items proposés|Validation|Feedback associé|Complément d'information|
|---|:---:|---|---|
|Il veut générer des playlists thématiques.|Oui|Pour cela, Guillaume aura besoin d'organiser les informations liées à ses fichiers musicaux.| |
|Il veut les changer de format.| |Ces fichiers sont encodés en mp3, ce qui est courant pour les fichiers musicaux. Guillaume n'a pas parlé de changer cela. Guillaume n'a pas parlé de changer cela.|L'extension du nom d'un fichier donne des indications sur son contenu. D'autres extensions courantes pour les fichiers musicaux comme .wma ou .ogg|
|Il veut les partager avec ses amis.| |Attention au respect des droits sur les oeuvres ! Mais heureusement, ce n'est pas ce que Guillaume a prévu de faire.|Les droits sur les oeuvres sont régis par le Code de la propriété intellectuelle.|
|Il veut les classer par album.| |Les fichiers mucicaux de l'amie de Guillaume sont déjà classés par album dans sa discothèque.| |
	

			
			
## 2) Informatique
**Que veut dire "informatique" ?**

|Items proposés|Validation|Feedback associé|Complément d'information|
|---|:---:|---|---|
|Traitement automatique de l'information.|Oui|Le mot "informatique" est un mot-valise formé à partir les mots "information" et "automatique". Il désigne le traitement automatique des données par des machines.|On distingue "informatique" et "numérique". Le "numérique" fait référence à la numérisation généralisée des données, c'est à dire à leur représentation sous forme de nombres qui peuvent être traités par des machines.|
|Utilisation d'un ordinateur.||Il est possible d'utiliser un ordinateur sans vraiment faire de l'informatique, par exemple lorsqu'on regarde une vidéo sur son ordinateur.|On est alors dans le champ de l'usage des technologies numériques.|
|Navigation sur internet.||Internet fait référence à l'architecture matérielle et logicielle qui permet la circulation des informations au niveau mondial.L'informatique est nécessaire au fonctionnement d'Internet, mais l'informatique est beaucoup plus large. On peut faire de l'informatique hors-ligne par exemple.||
|Installation et réparation d'un ordinateur et de ses périphériques (imprimante,...).||On parle alors de maintenance du matériel informatique, pas d'informatique en général.||

## 3) Donnée
**Qu'est-ce qu'une donnée dans le contexte de la vidéo ?**

|Items proposés|Validation|Feedback associé|Complément d'information|
|---|:---:|---|---|
|Une valeur décrivant un objet.|	Oui	|C'est la dénition qui sera retenue en SNT.|Une donnée est une valeur décrivant un objet, une personne, un événement digne d’intérêt pour celui qui choisit de la conserver.|
|La représentation conventionnelle d'une information en vue de son traitement informatique.|	Oui	|C'est la définition du dictionnaire Larousse.	| |
|Un élément que l'on connaît et qui sert de point de départ à un raisonnement ou à une recherche.|		|Le mot "donnée" peut avoir ce sens, mais dans un autre contexte.| |	
|Une hypothèse figurant dans un énoncé de mathématiques.| |		Ce serait exact dans le contexte d'un problème de mathématique, mais dans dans le contexte de cette vidéo.| |

## 4) Types de données
**À quelle condition peut-on comparer des données ?**

|Items proposés|Validation|Feedback associé|Complément d'information|
|---|:---:|---|---|
|On peut comparer des données si elles sont du même type.| Oui  |Les données peuvent être de différents types : texte, nombre, date,... Mais on ne peut comparer entre elles que des données du même type, des dates entre elles par exemple.| |	
|On peut comparer des données si elles ne sont pas trop nombreuses.| |		Au contraire, l'informatique permet de comparer entre elles un grand nombre de données, et très rapidement !| |	
|On peut toujours comparer des données.||		Comment compareriez-vous une date et une couleur ?	||
|On peut comparer des données seulement si ce sont des nombres.||		Avec l'ordre alphabétique, comparer deux mots n'est pas possible ?	||


## 5) Données structurées et données non structurées
**Pourquoi structurer les données ?**

|Items proposés|Validation|Feedback associé|Complément d'information|
|---|:---:|---|---|
|Pour pouvoir retrouver facilement des informations.|Oui|		On structure les données de manière à pouvoir y accéder beaucoup plus facilement. Le rangement adopté facilite l'accès aux données.	|Structurer correctement les données garantit que l’on puisse les exploiter facilement pour produire de l’information.|
|Pour pouvoir les traiter automatiquement.|Oui|		Lorsque les données sont structurées, on peut déléguer certaines tâches fastidieuses de recherche ou de traitement aux machines.||	
|Pour que ce soit agréable à regarder.||		Ce n'est pas le but premier de la structuration des données, mais la recherche de visualisations adaptées fait partie du traitement des données structurées.||	
|Pour pouvoir les stocker sur une machine.	||	On stocke aussi beaucoup de données non structurées sur des machines : textes, contenus vidéo...|Les données non structurées peuvent aussi être exploitées, par exemple par les moteurs de recherche.|

## 6) Table de données
**Qu'est-ce qu'une table de données ?**

|Items proposés|Validation|Feedback associé|Complément d'information|
|---|:---:|---|---|
|Un ensemble de données organisées sous forme de tableau.|Oui|		Les colonnes correspondent aux catégories de données, ou descripteurs.Les lignes correspondent aux différents objets rangés dans la table.|La structure de table permet de présenter une collection : les objets en ligne, les descripteurs en colonne et les données à l’intersection. Les données sont alors dites structurées.<br>Dans une table, la première ligne a un statut particulier. Elle comporte le nom des descipteurs et sert à décrire la forme des lignes suivantes.|
|Un ensemble d'informations concernant la confection des repas.||		Dans ce contexte, le mot "table" n'a rien à voir avec les repas.||	
|Un ensemble d'instruments qu'on utilise sur une table de cuisine, comme une moulinette.||		Dans ce contexte, le mot "table" n'est pas un meuble. Et ici, la moulinette est un programme informatique, et non un ustensile de cuisine.	||
|Un programme qui permet de dire si une information est vraie ou fausse.||		Il ne s'agit pas d'une table au sens table de vérité comme en logique mathématique.	||

## 7) Indexation des données
**Où peut-on trouver les informations pour remplir une table ?**

|Items proposés|Validation|Feedback associé|Complément d'information|
|---|:---:|---|---|
|Dans les métadonnées qui accompagnent tout fichier numérique.|Oui|		Les métadonnées sont des données sur les données. Elles permettent de décrire le contenu des fichiers.	|À tout fichier sont associées des métadonnées qui permettent d’en décrire le contenu. Ces métadonnées varient selon le type de fichier (date et coordonnées de géolocalisation d’une photographie, auteur et titre d’un fichier texte, etc.).|
|Dans des fichiers de données ouvertes.	|Oui|	Par exemple, data.gouv.fr est un site de données ouvertes (ou open data).|Certaines données sont dites ouvertes (OpenData), leurs producteurs considérant qu’il s’agit d’un bien commun.|
|En consultant le contenu de chaque fichier.||		Comment trouverez-vous le nom d'un interprète en écoutant un fichier musical ? sauf à le reconnaître à la voix !||	
|En cherchant chaque information dont on a besoin avec un moteur de recherche.	||	Cela peut convenir pour essayer de trouver quelques informations manquantes. Mais ce serait beaucoup trop fastidieux et chronophage de procéder de cette manière pour chaque information.	||

## 8) Opérations sur une table
**Quelles sont les opérations que l'on peut faire sur une table ?**

|Items proposés|Validation|Feedback associé|Complément d'information|
|---|:---:|---|---|
|Rechercher tous les objets qui répondent à un critère.|Oui|		Cela s'appelle filtrer des données.	||
|Trier.	|Oui|	Par exemple les titres par ordre de sortie, les interprètes par ordre alphabétique...||
|Faire des calculs.	|Oui|	Par exemple, calculer la durée totale d'une play list.||
|Fusionner des colonnes.||		On ne peut pas avoir deux descripteurs dans la même colonne.|Attention à la confusion entre les opérations sur les données et les fonctionnalités d'un tableur.|

## 9) Base de données relationnelles
**Pourquoi créer plusieurs tables ?**

|Items proposés|Validation|Feedback associé|Complément d'information|
|---|:---:|---|---|
|Pour éviter les redondances et n'avoir à mettre à jour qu'une seule fois chaque valeur en cas de changement.|Oui|		On essaie d'éviter le travail inutile !	||
|Pour obtenir de nouvelles informations.|Oui|		Les différentes tables crées constituent une base de données relationnelle.	|Une base de données regroupe plusieurs collections de données reliées entre elles.|
|Pour pouvoir croiser ces tables si elle possède un descripteur commun.	|Oui|	L'opération qui consiste à obtenir une nouvelle table en croisant deux tables qui possèdent un descripteur commun s'appelle la jointure.	|La recherche dans une base comportant plusieurs collections peut aussi croiser des collections différentes sur un descripteur commun ou comparable.|
|C'est à éviter : il est préférable de disposer de toutes les informations dans la même table.||		Non, non, ici ranger les données dans plusieurs tables est une bonne idée !	||

## 10) Expérimentation
**Comment un chercheur a-t-il montré qu'il était possible d'accéder à des données médicales individuelles ?**

|Items proposés|Validation|Feedback associé|Complément d'information|
|---|:---:|---|---|
|En croisant une liste électorale avec une base de données médicales ne comportant pourtant pas de noms.|Oui|		En effet, en croisant deux tables, on peut générer des informations nouvelles. Anonymiser des données n'est donc pas simple...	| Une activité de ce type est proposée dans la partie PRENDRE CONSCIENCE DES ENJEUX sur Algorea (tout en bas dans ce chapitre).|
|En s'introduisant dans la base de données de l'hôpital, qui n'était pas sécurisée.	||	Des failles de sécurité pourraient être exploitées par des hackers, malveillants, mais ce chercheur n'a pas mobilisé de compétences avancées en informatique.||	
|En demandant ses informations médicales à chaque patient.	||	Non, cela s'est fait à l'insu des patients.	||
|En tapant les bons mots clés dans un moteur de recherche, il a obtenu directement un document avec des données médicales individuelles.||		C'est très peu probable que ce chercheur ait réussi de cette manière. Il faudrait qu'un document comportant les données médicales individuelles soit déjà librement accessible en ligne.||

## 11) Réglement Général sur la Protection des Données
**Quel est l'enjeu du RGPD cité dans la vidéo ?**

|Items proposés|Validation|Feedback associé|Complément d'information|
|---|:---:|---|---|
|Préserver l'anonymat des sujets participant à des recherches.|Oui|		C'est l'enjeu cité dans la vidéo, mais on trouve des enjeux liés au Réglement Général pour la Protection des Données (RGPD) dans de nombreux domaines.	|On assiste au développement d’un marché de la donnée où des entreprises collectent et revendent des données sans transparence pour les usagers. D’où l’importance d’un cadre juridique permettant de protéger les usagers, préoccupation à laquelle répond le règlement général sur la protection des données (RGPD).|
|Mieux protéger les données personnelles des citoyens européens.|Oui|		C'est le principal enjeu du RGPD. Dans la vidéo, cet enjeu est présenté dans le domaine de la recherche.||
|Informer les utilisateurs de sites Web sur la collecte de données les concernant.||		Ce n'est pas abordé dans la vidéo, mais c'est un autre enjeu du RGPD.|
|Lutter contre les copies illicites d'oeuvres musicales.||		Cet enjeu ne relève pas du RGPD.||	