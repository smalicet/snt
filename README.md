# **SNT**

*Cours de SNT pour les secondes*

<source src="https://static.genial.ly/resources/panel-loader-low.mp4" type="video/mp4" />

Ici vous trouverez les dernières actualités avec notamment les travaux à réaliser.<br>
Cette page sera mise à jour à chaque fin de visioconférence ou de période de devoirs.<br>
**Je vous conseille de la mettre dans les favoris de votre ordinateur afin de la retrouver facilement.**  
Nous allons profiter de cette période pour travailler via la plateforme [**parcours 
algorea**
](http://parcours.algorea.org/)
 qui permet un travail de qualité grâce à l'aide de cours et exercices interactifs.<br>
Je ferai de temps à autre des réunions sur le site du [**CNED : ma classe à la maison**](https://lycee.cned.fr/).  
Je suis sur le réseau social [**Discord**](https://discordapp.com/) avec pas mal d'autres élèves (principalement pour les premières et terminales) : Vous pouvez rejoindre ce salon intitulé SNT pour rentrer en contact avec moi via ca lien : [https://discord.gg/5rRKYmW](https://discord.gg/5rRKYmW)

# **Modalités :**

### **Parcours Algorea :**
Je vous rappelle le lien du site : [Parcours Algorea](http://parcours.algorea.org/)<br>
Pour chaque session, il vous faudra être bien connecté à votre compte qui doit 
être relié à l'espace classe. <br>
Pour voir si vous êtes dans l'espace classe, il vous 
suffit d'aller dans  votre compte : **Profil > Mes Groupes**<br>
Doit y figurer Bavay_2A par exemple pour un élève en 2A.<br>
Sinon vous devez rejoindre le groupe classe. Je vous redonne des codes d'accès si 
vous devez vous recréer un compte ou avez perdu vos identifiants :
*  Bavay_2A : s9f7r54nr9
*  Bavay_2B : kkxpi5fww5
*  Bavay_2D : im6hdacr8t
*  Bavay_2E : 7tntwh5c8q

### **Ma classe à la maison :**
Allez sur le site du CNED : [https://lycee.cned.fr](https://lycee.cned.fr/)  
Créez vous-y un compte qui vous servira peut-être avec d'autres enseignants également.  
Connectez-vous à la date et heure indiquée par moi-même en cliquant sur le lien que je mettrai ici :  
**Prochain rendez-vous :** Mercredi 27 mai à 15h00  
**Lien de connection :** [Ma classe virtuelle](https://eu.bbcollab.com/guest/01b3df29bd10494fbedfd457ce9c6d30)



# **Période du 16 au 25 mars :**
### **Travail à réaliser : *environ 2h***  
On va poursuivre dans le nouveau chapitre entamé la semaine précédente : ***Traitement des données structurées***
1. Pour ceux qui n'ont pas encore fini le questionnaire : <br>
Je vous joins [les réponses](https://framagit.org/smalicet/snt/-/blob/5dd84e8de063f05236517f3677456dbdc3aaec36/Traitement%20des%20donn%C3%A9es%20structur%C3%A9es/correction_quizz.md) que vous prendrez soin d'analyser puisqu'elles seront au programme d'un prochain devoir surveillé.
  
2. Les exercices sur les listes déroulantes :<br>
**Progresser > SNT > Données structurées et leur traitement > DÉCOUVRIR : effectuer des requêtes à l'aide de listes déroulantes**  
Il y a un exercice qui disfonctionne et pour lequel vous n'arriverez pas à obtenir tous les points :  
![image](./jeretiens_untri.png)
  
3. Toute la partie sur le tableur :
Il y a :  
* une vidéo explicative  
* une manipulation pour ouvrir un fichier .csv avec un tableur (vous pouvez télécharger libreoffice [ici](https://fr.libreoffice.org/download/telecharger-libreoffice/))
* une série de manipulations du fichier à l'aide du tableur (exercices Musique 1 à 6)  
**Progresser > SNT > Données structurées et leur traitement > UTILISER un outil courant : le tableur** 
  
4. L'exercice sur le croisement de données qui permet de lever l'anonymat : <br>
**Progresser > SNT > Données structurées et leur traitement > PRENDRE CONSCIENCE DES ENJEUX > Anonyme, vraiment ?**
  
5. Travail facultatif : Faire la partie Python des données structurées :   
Voici un exemple pour l'exercice le plus difficile *(les capitales de région)* avec les commentaires expliquant quelle est l'utilité de cette ligne de code :
```python 
from database import *
table1 = loadTable('regions') # on importe le fichier regions dans une variable appelée table1
table2 = loadTable('villes') # on importe le fichier villes dans une variable appelée table2
table = joinTables(table1, 'capitale', table2, 'ville','inner') # on réunit les deux tables en faisant coincider les capitales avec les villes
table = selectColumns(table,['table1_capitale','table1_region','table2_departement','table2_nb_habitants']) # on ne garde que les 4 colonnes qui nous intéressent
table = sortByColumn(table,'table1_capitale','asc') # on tri notre tableau par rapport aux capitales par ordre croissant (alphabétique)
displayTable(table) # on affiche le tableau
```
***Ce travail sera évalué et donnera lieu à une note.  
J'ai bien conscience que pour certains d'entre vous il sera difficile d'avoir accès à
un ordinateur et un délai supplémentaire peut être accordé.***  

# **Période du 26 au 30 mars :**
  
On change de chapitre et on va s'intéresser à la **Photographie Numérique** désormais.  
J'ai choisi de changer de progression et que l'on se plonge dans ce chapitre puisqu'il est accessible à distance si vous disposez d'un ordinateur.  
On travaille encore sur [**parcours algorea**](http://parcours.algorea.org/).  
***N'oubliez pas de vous connecter à votre compte pour que je suivre le travail fait.***  
D'ailleurs d'ici la semaine prochaine je rentrerai vos notes pour la partie sur les données structurées.  

### **Travail à réaliser : *environ 30 minutes***  

[**Progresser > SNT > Photo Numérique > Introduction**](https://parcours.algorea.org/contents/4707-4702-1352246428241737349-1564577382617336413-458845999776651154/)  
Regarder la vidéo d'introduction et faites le quizz correspondant.  
Bon travail.

*Il se peut que certains n'obtiennent pas 100% même en ayant toutes les bonnes réponses. Ce n'est pas bien grave vu que cette partie la sera réévaluée lors d'un DS à la reprise.*
  

# **Période du 1 au 10 avril :**
  
On poursuit le travail sur la **Photographie Numérique** désormais.  
On travaille encore sur [**parcours algorea**](http://parcours.algorea.org/).  
***N'oubliez pas de vous connecter à votre compte pour que je puisse suivre le travail fait.***  
Certains élèves ont eu 0/20 en ayant fait le travail précédent car si vous n'avez pas associer votre compte au groupe classe je ne peux pas deviner de chez moi que vous avez fait le travail.  
Je vous rappelle d'ailleurs la procédure :  
> ### **Parcours Algorea :**
> Je vous rappelle le lien du site : [Parcours Algorea](http://parcours.algorea.org/)<br>
> Pour chaque session, il vous faudra être bien connecté à votre compte qui doit 
> être relié à l'espace classe. <br>
> Pour voir si vous êtes dans l'espace classe, il vous 
> suffit d'aller dans  votre compte : **Profil > Mes Groupes**<br>
> Doit y figurer Bavay_2A par exemple pour un élève en 2A.<br>
> Sinon vous devez rejoindre le groupe classe. Je vous redonne des codes d'accès si 
> vous devez vous recréer un compte ou avez perdu vos identifiants :
> *  Bavay_2A : s9f7r54nr9
> *  Bavay_2B : kkxpi5fww5
> *  Bavay_2D : im6hdacr8t
> *  Bavay_2E : 7tntwh5c8q
  
D'autre part pour les éventuelles raisons de votre non travail, si celles-ci sont justifiées il sera bien évidemment possible de revenir sur ce 0 (en vous laissant faire le travail au CDI par exemple sur votre temps libre) qui est surtout le signe que je n'ai pas eu de travail.  
D'autres évaluations, en classe, auront bien entendu lieu à la reprise notamment pour évaluer la compréhension des vidéos et autres petits quizz sur les notions travaillées.   

### **Travail à réaliser : *environ 2 heures***  
  
### Partie 1 : 1 heure
[**Progresser > SNT > Photo Numérique > DÉCOUVRIR quelques caractéristiques des images numériques**](https://parcours.algorea.org/contents/4707-4702-1352246428241737349-1564577382617336413-1452659997448273932/)  
Il s'agit de faire tous les exercices disponibles.  

Vous avez jusqu'à jeudi 9 avril inclus pour réaliser tous les exercices qui seront encore une fois évalué.  

*Le score maximal possible est 100% sur algorea:*  
![](./photonumerique.png)  

### Partie 2 : 1 heure  
**le vendredi 10 avril : Cours en visio**  

***Objectif : On fera ensemble des petits quizz sur les activités réalisées lors des exercices sur la photo numérique.***  
  
2 sessions (inutile de venir aux deux):  
La première sur discord (de 10h à 11h) puis une seconde en classe virtuelle(de 14h00 à 15h00)  
  


# Période du 28 avril au 7 mai :  

On va travailler sur les jupyter notebooks afin de s'améliorer dans notre maîtrise de Python, très utile en mathématiques, informatiques et sciences.  

On va passer par Binder.  
Binder crée un ordinateur virtuel qui exécute le notebook et affiche le résultat sur votre navigateur.  
  
Premier TD python : Les variables (vous verrez, il y a pas mal de rappels mais cela permet de repartir sur de bonnes bases)  
Vous pouvez y accéder directement en cliquant ici : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fsmalicet%2Fsnt/master?filepath=%2Fnotebooks%2Fles_variables.ipynb)  
  
*Attention il faut parfois attendre 2-3 minutes pour que l'ordi virtuel soit créé*   

*N'oubliez pas de sauvegarder votre jupyter notebook une fois celui ci finit :* **File > Download as > Notebook** 
  

# Période du 11 au 15 mai :  

On va poursuivre les travaux en python.  
Voici un notebook sur **les tests** : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fsmalicet%2Fsnt/master?filepath=%2Fnotebooks%2Fles_tests.ipynb)

Certains exercices peuvent paraître difficile - l'exercice 4 et le 6 notamment - n'hésitez pas à les passer si vous avez du mal toutefois les autres exercices sont à la **portée de tous**.  

# Période du 18 au 20 mai :  

RDV en classe virtuelle pour la correction du TD python.  
On met python de côté cette semaine pour se consacrer à un nouveau thème du programme : **Localisation, cartographie, mobilité**  
  
Je vous propose un petit TD pour la compréhension du fonctionnement d'un GPS.  
Téléchargez et imprimer la **[carte](/PDF/carte_gps.pdf)**  
Voici le **[sujet du TD](/PDF/ex_gps.pdf)**    
Les 3 premières partie vous plongent dans l'histoire, lisez les très attentivement afin de bien comprendre le problème et la méthode puis à la partie 4 ce sera à vous de jouer.  

On corrigera cette activité la semaine prochaine.  
Bien entendu je compte sur vous pour m'envoyer vos travaux en email : sebmalicet@hotmail.com ou sur discord car votre investissement sera valorisé sur vos bulletins.


## Voici le prochain RDV "classe virtuelle" :  
* Mercredi 27 mai 15h sur classe virtuelle : [Ma classe virtuelle](https://eu.bbcollab.com/guest/01b3df29bd10494fbedfd457ce9c6d30)


# Période du 25 au 29 mai :
  
RDV en classe virtuelle pour correction du TD  
[Bilan du TD](/PDF/ex_gps.pdf)  
[Vidéo d'explication du GPS](https://parcours.algorea.org/contents/4707-4702-1352246428241737349-873755626585744914-620528054155816023-1813392980826652291/)  
[Questionnaire sur le GPS](https://parcours.algorea.org/contents/4707-4702-1352246428241737349-873755626585744914-620528054155816023-4411323387616591/)  
  
Une application en python :  
  
```python
import webbrowser
zoom='18'
lat=50.298602 
lon=3.7951583
webbrowser.open('https://www.openstreetmap.org/note/new?lat='+str(lat)+'&lon='+str(lon)+'#map='+zoom+'/'+str(lat)+'/'+str(lon))
```
  
**Travail à faire :**   
_Ce travail sera présenté lors de la séance de classe virtuelle._  
Cherchez les coordonnées GPS d'un monument de votre choix.  
En modifiant le code donné ci-dessus, afficher directement votre lieu.  
Envoyez moi un screenshot de votre algorithme + le rendu visuel comme dans l'exemple ci-dessous :  
![exemple](exemplelat.jpg)  





