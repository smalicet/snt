from microbit import *

#boucle infinie
while True:
    #Structure conditionnelle avec 3 choix
    if button_a.is_pressed():
        display.show(Image.HAPPY)
    elif button_b.is_pressed():
        display.show(Image.ANGRY)
    else:
        display.show(Image.SAD)

