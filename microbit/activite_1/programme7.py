from microbit import *

#Calibrage du compas
compass.calibrate()

#boucle
while not button_a.is_pressed():
    angle = compass.heading()
    if 315 <= angle or angle <= 45:
        display.show("N")
    elif 45 < angle and angle <= 135:
        display.show("E")
    elif 135 < angle and angle <= 225:
        display.show("S")
    elif 225 < angle and angle <= 315:
        display.show("O")
    #attente d’une seconde
    sleep(1000)

display.clear()


