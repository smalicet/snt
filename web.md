Voici ci-dessous la liste des élèves et le nom de la page web  / fichier style qu'ils doivent choisir :


## Classe : 2 B

Thème : Sportifs

Prénom de l'élève | page web | fichier style
---:|:--:|:---
Céleste|bascou.html|bascou.css
Rémi|mbappe.html|mbappe.css
enzo|mcgregor.html|mcgregor.css
matheo|nishida.html|nishida.css
justin|tyson.html|tyson.css
cyrielle|mayer.html|mayer.css
clara|otto.html|otto.css
juliette|ohashi.html|ohashi.css
quentin|parker.html|parker.css
kevin|lavillenie.html|lavillenie.css
laura|marquez.html|marquez.css
chloe|zidane.html|zidane.css
alicia|varane.html|varane.css
lea|maradona.html|maradona.css
emmanuel|marhez.html|marhez.css
valentine|hernandes.html|hernandes.css
howahkan|rinner.html|rinner.css
manon|bolt.html|bolt.css
clement|woods.html|woods.css
thomas|nadal.html|nadal.css
andrea|diaz.html|diaz.css
lisa|jordan.html|jordan.css
camille|djokovich.html|djokovich.css
paul|thiem.html|thiem.css
théo|williams.html|williams.css



## Classe : 2 C

Thème : Artistes de musique


Prénom de l'élève | page web | fichier style
---:|:--:|:---
Léa|bigflo.html|bigflo.css
Baptiste|zola.html|zola.css
Marwen|cloclo.html|cloclo.css
thomas|eminem.html|eminem.css
clementine|rostropovitch.html|rostropovitch.css
gauthier|rk.html|rk.css
yasmine|eilish.html|eilish.css
julie|beyonce.html|beyonce.css
gaspard|kobalad.html|kobalad.css
marceau|mozard.html|mozard.css
alexandre|traviscott.html|traviscott.css
marion|jul.html|jul.css
yann|choupi.html|choupi.css
lauréline|imaginedragons.html|imaginedragons.css
camille|u2.html|u2.css
coline|acdc.html|acdc.css
sarah|jackson.html|jackson.css
noemie|lomepal.html|lomepal.css
kilian|crazyfrog.html|crazyfrog.css
matheo|xxxtentation.html|xxxtentation.css
nathan|pnl.html|pnl.css
manon|ninho.html|ninho.css
chaynese|djadjadinaz.html|djadjadinaz.css
margaux|nekfeu.html|nekfeu.css



## Classe : 2 E

Thème : Animaux

Prénom de l'élève | page web | fichier style
---:|:--:|:---
ophelie|dauphin.html|dauphin.css
lea|loup.html|loup.css
lola|pingouin.html|pingouin.css
zoe|renardpolaire.html|renardpolaire.css
amandine|tigre.html|tigre.css
martin|colibri.html|colibri.css
garance|lion.html|lion.css
océane|irishcob.html|irishcob.css
audrey|cheval.html|cheval.css
mathis|ocelot.html|ocelot.css
clementine|manchot.html|manchot.css
lola|loutre.html|loutre.css
fanny|paresseux.html|paresseux.css
rulyann|chien.html|chien.css
fouad|rat.html|rat.css
capucine|chat.html|chat.css
baptiste|koala.html|koala.css
clarys|panda.html|panda.css
olivier|mouflon.html|mouflon.css
alexis|ecureuil.html|ecureuil.css
jeremy|mouton.html|mouton.css
mathys|gorille.html|gorille.css
lucie|coyotte.html|coyotte.css
arthur|orangoutan.html|orangoutan.css
louis|elephant.html|elephant.css